import React, { useEffect, useState } from "react";
import {
  Box,
  InputLeftElement,
  InputGroup,
  Input,
  Button,
} from "@chakra-ui/react";
import { GrMapLocation } from "react-icons/gr";

export default function SearchBar(props) {
  const [address, setAddress] = useState(null);
  const handleAddress = (e) => {
    setAddress(e.target.value);
  };
  const FindAddress = (e) => {
    props.searchAddress(address);
  };
  return (
    <InputGroup>
      <InputLeftElement pointerEvents="none" children={<GrMapLocation />} />
      <Input
        type="addr"
        placeholder="Address"
        value={address}
        onChange={handleAddress}
        bg={"white"}
        color="black"
      ></Input>
      <Button onClick={FindAddress} colorScheme="blue">
        Search
      </Button>
    </InputGroup>
  );
}
