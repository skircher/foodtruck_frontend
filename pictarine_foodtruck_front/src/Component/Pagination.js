import React from "react";
import { Box, Button } from "@chakra-ui/react";
import { GrNext } from "react-icons/gr";
import { GrPrevious } from "react-icons/gr";
import { ImNext2 } from "react-icons/im";
import { ImPrevious2 } from "react-icons/im";

export default function Pagination(props) {
  const handleChangeNext = () => {
    props.changePage(props.currentPage + 1);
  };
  const handleChangePrev = () => {
    props.changePage(props.currentPage - 1);
  };
  const handleChangeGoToStart = () => {
    props.changePage(0);
  };
  const handleChangeGoToEnd = () => {
    props.changePage(props.nombrePage - 1);
  };
  return (
    <Box>
      <Button colorScheme="blue" mr={1} mt={2} onClick={handleChangeGoToStart}>
        <ImPrevious2 />
      </Button>

      <Button
        colorScheme="blue"
        mr={1}
        mt={2}
        onClick={handleChangePrev}
        disabled={props.currentPage == 0 ? true : false}
      >
        <GrPrevious />
      </Button>

      <Button
        colorScheme="blue"
        mr={1}
        mt={2}
        onClick={handleChangeNext}
        disabled={props.currentPage == props.nombrePage - 1 ? true : false}
      >
        <GrNext />
      </Button>

      <Button colorScheme="blue" mr={1} mt={2} onClick={handleChangeGoToEnd}>
        <ImNext2 />
      </Button>
    </Box>
  );
}
