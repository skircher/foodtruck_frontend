import React from "react";
import {
  Box,
  Image,
  Text,
  SimpleGrid,
  Heading,
  Flex,
  Grid,
  GridItem,
} from "@chakra-ui/react";
import { GiHotMeal } from "react-icons/gi";
import { GiThermometerCold } from "react-icons/gi";
import { BiMap } from "react-icons/bi";
import { GiKnifeFork } from "react-icons/gi";
import { BiDrink } from "react-icons/bi";
import { BiWorld } from "react-icons/bi";

export default function DescFoodTruck(props) {
  const HotColdFood = () => {
    let heatFood = [];
    if (props.foodTruckItem.foodItems.length > 0) {
      props.foodTruckItem.foodItems.filter((fooditem) =>
        heatFood.push(fooditem.foodHeat)
      );
    }
    let elemToReturn = null;
    if (heatFood.includes("Cold") && heatFood.includes("Hot")) {
      return (
        <Flex>
          <GiHotMeal color="red" />
          <GiThermometerCold color="blue" />
        </Flex>
      );
    } else if (heatFood.includes("Cold")) {
      return <GiThermometerCold color="blue" />;
    } else if (heatFood.includes("Hot")) {
      return (
        <Box>
          <GiHotMeal color="red" />
        </Box>
      );
    }
  };
  const ListFoodtems = (typeFood) => {
    return props.foodTruckItem.foodItems.length > 0
      ? props.foodTruckItem.foodItems
          .filter((fooditem) => fooditem.foodState.includes(typeFood))
          .map(function (name) {
            return name["name"];
          })
          .map((value, index) => {
            return <Text ml={2}>{value}</Text>;
          })
      : null;
  };
  const ListNationalitiesFood = () => {
    return props.foodTruckItem.nationalities
      ? props.foodTruckItem.nationalities.length > 0
        ? props.foodTruckItem.nationalities.map((value, index) => {
            return <Text ml={2}>{value}</Text>;
          })
        : null
      : null;
  };
  const ParticularitiesFood = () => {
    if (props.foodTruckItem.foodType)
      if (props.foodTruckItem.foodType.length > 0) {
        return (
          <Box>
            <Text as="b">Particularities : </Text>
            <SimpleGrid>{props.foodTruckItem.foodType}</SimpleGrid>
          </Box>
        );
      }
  };
  return (
    <Box mt={5}>
      <Grid
        h="200px"
        templateRows="repeat(5, 1fr)"
        templateColumns="repeat(3, 1fr)"
        gap={4}
      >
        <GridItem rowSpan={1} colSpan={4} as={"b"} mb={5}>
          <Heading textAlign="center">
            {props.foodTruckItem.applicant.substring(0, 30)}
          </Heading>
        </GridItem>
        <GridItem rowSpan={4} colSpan={1}>
          <Image height="60%" src={props.image} alt="facilityType" />
        </GridItem>
        <GridItem rowSpan={4} colSpan={3}>
          <Flex>
            <BiMap />
            <Text>{props.foodTruckItem.address}</Text>
          </Flex>
          {HotColdFood()}
          <Box>
            <GiKnifeFork />
            <SimpleGrid columns={3}>{ListFoodtems("Solid")}</SimpleGrid>
          </Box>
          <Box>
            <BiDrink />
            <SimpleGrid columns={3}>{ListFoodtems("Drink")}</SimpleGrid>
          </Box>
          <Box>
            <BiWorld />
            <SimpleGrid columns={3}>{ListNationalitiesFood()}</SimpleGrid>
          </Box>
          {ParticularitiesFood()}
          <Box>
            <Text as={"b"}>Description : </Text>
            <Text>{props.foodTruckItem.description}</Text>
          </Box>
        </GridItem>
      </Grid>
    </Box>
  );
}
