import {
  Box,
  Image,
  Text,
  SimpleGrid,
  Flex,
  Grid,
  GridItem,
} from "@chakra-ui/react";
import { GiHotMeal } from "react-icons/gi";
import { GiThermometerCold } from "react-icons/gi";
import { BiMap } from "react-icons/bi";
import { GiKnifeFork } from "react-icons/gi";
import { BiDrink } from "react-icons/bi";
import { BiWorld } from "react-icons/bi";

export default function BlocClient(props) {
  const HotColdFood = () => {
    let heatFood = [];
    if (props.foodTruckItem.foodItems.length > 0) {
      props.foodTruckItem.foodItems.filter((fooditem) =>
        heatFood.push(fooditem.foodHeat)
      );
    }
    let elemToReturn = null;
    if (heatFood.includes("Cold") && heatFood.includes("Hot")) {
      return (
        <Flex>
          <GiHotMeal color="red" />
          <GiThermometerCold color="blue" />
        </Flex>
      );
    } else if (heatFood.includes("Cold")) {
      return <GiThermometerCold color="blue" />;
    } else if (heatFood.includes("Hot")) {
      return (
        <Box>
          <GiHotMeal color="red" />
        </Box>
      );
    }
  };
  const ListFoodtems = (typeFood) => {
    return props.foodTruckItem.foodItems.length > 0
      ? props.foodTruckItem.foodItems
          .filter((fooditem) => fooditem.foodState.includes(typeFood))
          .map(function (name) {
            return name["name"];
          })
          .map((value, index) => {
            return <Text ml={2}>{value.substring(0, 9)}</Text>;
          })
      : null;
  };
  const ListNationalitiesFood = () => {
    return props.foodTruckItem.nationalities.length > 0
      ? props.foodTruckItem.nationalities.map((value, index) => {
          return <Text ml={2}>{value}</Text>;
        })
      : null;
  };

  const selectCard = (e) => {
    props.selectFoodTruck(props.foodTruckItem);
  };

  return (
    <Box
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      _hover={{ backgroundColor: "rgb(189,66,32)", color: "white" }}
      onClick={selectCard}
    >
      <Box fontSize={"sm"}>
        <Grid
          h="200px"
          templateRows="repeat(5, 1fr)"
          templateColumns="repeat(3, 1fr)"
          gap={4}
          as={"b"}
        >
          <GridItem rowSpan={1} colSpan={3} bg={"rgb(189,66,32)"}>
            <Text textAlign="center" fontSize={"lg"} color={"white"}>
              {props.foodTruckItem.applicant.substring(0, 30).toUpperCase()}
            </Text>
          </GridItem>
          <GridItem rowSpan={4} colSpan={1}>
            <Image height="40%" src={props.image} alt="facilityType" ml={2} />
          </GridItem>
          <GridItem rowSpan={4} colSpan={2}>
            <Flex>
              <BiMap />
              <Text>{props.foodTruckItem.address}</Text>
            </Flex>
            {HotColdFood()}
            <Box>
              <GiKnifeFork />
              <SimpleGrid columns={3}>{ListFoodtems("Solid")}</SimpleGrid>
            </Box>
            <Box>
              <BiDrink />
              <SimpleGrid columns={3}>{ListFoodtems("Drink")}</SimpleGrid>
            </Box>
            <Box>
              <BiWorld />
              <SimpleGrid columns={3}>{ListNationalitiesFood()}</SimpleGrid>
            </Box>
          </GridItem>
        </Grid>
      </Box>
    </Box>
  );
}
