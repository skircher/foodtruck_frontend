import React, { useEffect, useState } from "react";
import {
  Box,
  Center,
  Grid,
  GridItem,
  Heading,
  RadioGroup,
  Radio,
  Stack,
  Image,
  Flex,
} from "@chakra-ui/react";
import ReactMapGL, { Marker, Popup } from "react-map-gl";
import { BiCart } from "react-icons/bi";
import { RiTruckLine } from "react-icons/ri";
import { FaMapMarkerAlt } from "react-icons/fa";
import { RiFacebookCircleFill } from "react-icons/ri";
import { AiFillInstagram } from "react-icons/ai";
import { AiFillTwitterCircle } from "react-icons/ai";
import { AiFillLinkedin } from "react-icons/ai";

import axios from "axios";

import ListFoodTruck from "../List/ListFoodTruck";
import DescFoodTruck from "../Component/DescFoodTruck";
import SearchBar from "../Component/SearchBar";

const MAPBOX_TOKEN =
  "pk.eyJ1Ijoic2tpcmNoZXIiLCJhIjoiY2tyN2hhazhlMDRqaDJ2bWMzY2o0cnB1dSJ9.rHV4ULkNbu2tHUhQW2yeSw";

export default function Home() {
  const [listFoodTruck, setListFoodTruck] = useState([]);
  const [showPopup, setPopup] = useState(null);
  const [radioFacility, setRadioFacility] = useState("none");
  const [searchPosition, setSearchPosition] = useState([37.7577, -122.4376]);
  const [viewport, setViewport] = useState({
    width: 700,
    height: 700,
    latitude: 37.7577,
    longitude: -122.4376,
    zoom: 11.5,
  });
  const facilityTypes = {
    Truck: "/images/truck_picture2.png",
    Push_Cart: "/images/cart_picture2.png",
  };
  const filterMobileFood = (dataTruck) => {
    if (radioFacility != "none") {
      return dataTruck.filter((ftruck) => ftruck.facilityType == radioFacility);
    } else {
      return null;
    }
  };
  useEffect(() => {
    let dataFilter = null;
    axios
      .get(`${process.env.REACT_APP_URL_BACKEND}/api/v1/foodtruck`)
      .then((response) => {
        dataFilter = filterMobileFood(response.data);
        setListFoodTruck(dataFilter == null ? response.data : dataFilter);
      });
  }, [radioFacility, viewport]);
  const MarkerFoodTruckItems = ({ children }) =>
    children.map((value, index) => {
      return (
        <Marker
          latitude={value.latitude}
          longitude={value.longitude}
          offsetLeft={-20}
          offsetTop={-10}
          onClick={() => setPopup(value)}
        >
          {value.facilityType == "Truck" ? <RiTruckLine /> : <BiCart />}
        </Marker>
      );
    });

  const FindAddress = (e) => {
    let addrFind = null;
    axios
      .get(
        `https://api.opencagedata.com/geocode/v1/json?q=${e} San Francisco&key=3a52b0eed5844ece921a2aa82bb0eb30`
      )
      .then((response) => {
        console.log(response.data.results);

        addrFind = response.data.results.filter((results) =>
          results.formatted.toUpperCase().includes("SAN FRANCISCO")
        );
        console.log(addrFind);

        if (addrFind.length > 0) {
          setViewport({
            width: 700,
            height: 700,
            latitude: response.data.results[0].geometry.lat,
            longitude: response.data.results[0].geometry.lng,
            zoom: 14,
          });
          setSearchPosition([
            response.data.results[0].geometry.lat,
            response.data.results[0].geometry.lng,
          ]);
        }
      });
  };

  return (
    <Box>
      <Grid
        h="200px"
        templateRows="repeat(5, 1fr)"
        templateColumns="repeat(5, 1fr)"
      >
        <GridItem rowSpan={1} colSpan={5} bg="rgb(189,66,32)">
          <Flex align="center" ml={5} mt={5} mb={5}>
            <Image
              height="7vh"
              src={"/images/logo_gsf.png"}
              alt="Segun Adebayo"
            />
            <Heading color="White">Golden Street Food</Heading>
          </Flex>
        </GridItem>
        <GridItem colSpan={1} bg="rgb(189,66,32)" color={"white"}>
          <Box padding={5}>
            <SearchBar value="15" searchAddress={FindAddress}></SearchBar>
          </Box>

          <RadioGroup
            padding={5}
            onChange={setRadioFacility}
            value={radioFacility}
          >
            <Stack direction="row" as="b">
              <Radio value="none">All</Radio>
              <Radio value="Push_Cart">Push Cart</Radio>
              <Radio value="Truck">Food Truck</Radio>
            </Stack>
          </RadioGroup>
        </GridItem>
        <GridItem colSpan={2} bg="rgb(248,179,35)">
          <Center mt={5}>
            <ReactMapGL
              {...viewport}
              onViewportChange={(nextViewport) => setViewport(nextViewport)}
              mapboxApiAccessToken={MAPBOX_TOKEN}
            >
              <MarkerFoodTruckItems>{listFoodTruck}</MarkerFoodTruckItems>
              {showPopup && (
                <Popup
                  latitude={showPopup ? showPopup.latitude : 0}
                  longitude={showPopup ? showPopup.longitude : 0}
                  closeOnClick={true}
                  onClose={() => setPopup(null)}
                  anchor="top"
                >
                  <div>{showPopup.applicant}</div>
                  <div>{showPopup.address}</div>
                  {showPopup.foodType ? (
                    <div>
                      Type :
                      {showPopup.foodType.map((value, index) => {
                        return <p>{value}</p>;
                      })}
                    </div>
                  ) : null}
                </Popup>
              )}
              <Marker
                latitude={searchPosition[0]}
                longitude={searchPosition[1]}
                offsetLeft={-20}
                offsetTop={-10}
              >
                <FaMapMarkerAlt color="red" />
              </Marker>
            </ReactMapGL>
          </Center>
        </GridItem>
        <GridItem colSpan={2} bg="rgb(248,179,35)">
          {showPopup == null ? (
            <ListFoodTruck
              filterFacility={radioFacility}
              foodtTruckSelect={setPopup}
            />
          ) : (
            <DescFoodTruck
              foodTruckItem={showPopup}
              image={facilityTypes[showPopup.facilityType]}
            />
          )}
        </GridItem>
        <GridItem rowSpan={1} colSpan={5} bg="rgb(189,66,32)">
          <Center>
            <Flex align="center" ml={5} mt={5} mb={5}>
              <Box height="6.5vh">
                <Flex align="center" ml={5} mt={5} mb={5}>
                  <RiFacebookCircleFill size="30" />
                  <AiFillInstagram size="30" />
                  <AiFillTwitterCircle size="30" />
                  <AiFillLinkedin size="30" />
                </Flex>
              </Box>
            </Flex>
          </Center>
        </GridItem>
      </Grid>
    </Box>
  );
}
