import React, { useEffect, useState } from "react";
import axios from "axios";
import { Box, SimpleGrid, Center } from "@chakra-ui/react";

import CardFoodTruck from "../Component/CardFoodTruck";
import Pagination from "../Component/Pagination";

export default function ListeClient(props) {
  const [listFoodTruck, setListFoodTruck] = useState([]);
  const [pageMax, setPageMax] = useState(0);
  const [pageNum, setPageNum] = useState(0);

  const facilityTypes = {
    Truck: "/images/truck_picture2.png",
    Push_Cart: "/images/cart_picture2.png",
  };

  const testButton = (e) => {
    props.foodtTruckSelect(e);
  };

  useEffect(() => {
    axios
      .get(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/foodtruck/list/${pageNum}/${props.filterFacility}`
      )
      .then((response) => {
        setListFoodTruck(response.data.content);
        setPageMax(response.data.totalPages);
      });
  }, [pageNum, props.filterFacility]);

  const FoodTruckItems = ({ children }) =>
    children.map((value, index) => {
      return (
        <CardFoodTruck
          foodTruckItem={value}
          image={facilityTypes[value.facilityType]}
          selectFoodTruck={testButton}
        />
      );
    });
  const handleClickPage = (value) => {
    setPageNum(value);
  };

  return (
    <Box>
      <Box padding={5}>
        <SimpleGrid
          columns={2}
          spacing={5}
          style={{ maxHeight: "85vh", overflow: "auto" }}
        >
          <FoodTruckItems>{listFoodTruck}</FoodTruckItems>
        </SimpleGrid>
        <Center>
          <Pagination
            nombrePage={pageMax}
            currentPage={pageNum}
            changePage={handleClickPage}
          ></Pagination>
        </Center>
      </Box>
    </Box>
  );
}
