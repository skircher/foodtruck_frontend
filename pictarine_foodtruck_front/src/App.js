import * as React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { ChakraProvider } from "@chakra-ui/react";

import Home from "./Page/Home";

function App() {
  return (
    <div className="App">
      <ChakraProvider>
        <Router>
          <Route exact path="/" component={Home} />
        </Router>
      </ChakraProvider>
    </div>
  );
}

export default App;
